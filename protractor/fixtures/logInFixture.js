module.exports.logInFixture = function () {
	
	this.usernameField = function () {
		return element(by.css('div.generic_formLayout input[name="cre:Username"]'));
	};
	
	this.passwordField = function () {
		return element(by.css('div.generic_formLayout input[name="cre:Password"]'));
	};
	
	this.submitButton = function () {
		return element(by.css('div.generic_formLayout button'));
	};
	
	this.notification = function () {
		return element(by.css('div.generic_formProcessResponse .responseError'));
	};
	
	this.logIn = function(username, password) {
		this.usernameField().sendKeys(username);
		this.passwordField().sendKeys(password);
		this.submitButton().click();
	};

};