'use strict';

/* global global, require, process, exports, browser, jasmine */
var arg = (function () {
	var args = process.argv.slice(2),
		params = {};

	for (var i = 0; i < args.length -1; i++) {
		params[args[i + 1].substring(2)] = args[i + 2];
		i++;
	}

	var conf = {
		output  		: params.output ? params.output : 'results/',
		environment 	: params.environment ? params.environment : 'test',
		collection  	: params.collection ? params.collection : 'AppBuilder',
		browser     	: params.browser ? params.browser : 'chrome',
		suite       	: params.suite ? params.suite : 'e2e',
		testdata       	: params.testdata ? params.testdata : 'klantportaal.tst'
	};

	return conf;
}());

var getTimeStamp = function() {
	var d = new Date();

	var formatDatePart = function(input) {
		var formatterInput = input < 10 ? '0' + input : input;
		return formatterInput;
	};

	return d.getFullYear() + formatDatePart(d.getMonth() + 1) + formatDatePart(d.getDate()) + '-' + formatDatePart(d.getHours()) + formatDatePart(d.getMinutes()) + formatDatePart(d.getSeconds());
};

var TIME_OUT  = 100000, // General wait (ms)
	BASE_URL  = 'https://klantportaal.tst.riviumba.com/',
	FILE_NAME = arg.environment + '-' + arg.collection + (arg.suite === '' ? '' : '-' + arg.suite) + '-' + getTimeStamp();

exports.config = {

	directConnect: true, //for newer versions of protractor

	capabilities: {
		'browserName': 'chrome'
	},

	specs: [
		'../specs/*Spec.js',
	],

	baseUrl: BASE_URL,

	onPrepare: function () {

		console.log('Using base URL ' + BASE_URL);
		console.log('Writing to ' + arg.output + FILE_NAME + '.xml');
		console.log('Testing with testdata "' + arg.testdata + '"');

		browser.ignoreSynchronization = true;

		// import libraries
		global.timeModule			= require('../libraries/timeModule.js');
		global.testDataModule		= require('../libraries/testDataModule.js');

		// import fixtures
		global.logInFixture 		= require('../fixtures/logInFixture.js').logInFixture;

		// convenience shortcuts
		global.baseUrl 				= BASE_URL;
		global.testdata 			= arg.testdata;
		global.wait		            = global.timeModule.wait;
		global.waitForElement		= global.timeModule.waitForElement;
		global.waitForClose 		= global.timeModule.waitForElementClose;
		global.waitForPageTitle 	= global.timeModule.waitForPageTitle;

		// import reporter
		require('jasmine-reporters');
		jasmine.getEnv().addReporter(new jasmine.JUnitXmlReporter(arg.output, true, true, FILE_NAME, true));

		var mkdirp = require('mkdirp');
		mkdirp(arg.output);

	},

	jasmineNodeOpts: {
		// onComplete will be called just before the driver quits.
		onComplete: null,
		// If true, display spec names.
		isVerbose: false,
		// If true, print colors to the terminal.
		showColors: true,
		// If true, include stack traces in failures.
		includeStackTrace: true,
		// Default time to wait in ms before a test fails.
		defaultTimeoutInterval: TIME_OUT
	}
};
