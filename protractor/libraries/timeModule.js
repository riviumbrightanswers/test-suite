/**
 * General timing related supporter library for Protractor environmental integration tests
 */

'use strict';

/* global module, browser */
var DEF_MIN_WAIT = 2000,  // general default minimum time (ms) to wait or sleep
	DEF_MAX_WAIT = 20000; // general default maximum time (ms) to wait before timeout

module.exports = {
	/**
	 * Wait synchronously for an amount of time
	 * @param [ms] {Number} the amount of time (ms) to wait
	 */
	wait: function (ms) {
		browser.sleep(ms || DEF_MIN_WAIT);
	},

	/**
	 * Wait synchronously for late appearing (asynchronously loaded) DOM elements
	 * @param elm {Object} the target DOM element to wait for
	 * @param [ms] {Number} optional: the amount of time (ms) to wait
	 */
	waitForElement: function (elm, ms) {
		browser.wait(function() {
			return elm.isPresent();
		}, ms || DEF_MAX_WAIT);
	},
	/**
	 * Wait synchronously for late appearing (asynchronously loaded) DOM element's text
	 * @param elm {Object} the target DOM element to wait for containing text
	 * @param [ms] {Number} optional: the amount of time (ms) to wait
	 */
	waitForElementText : function (elm, ms) {
		browser.wait(function () {
			return elm.getText().then(function (text) {
					return text.length > 0;
				}
			);
		}, ms|| DEF_MAX_WAIT);
	},
	/**
	 * Wait synchronously for page title
	 * @param [ms] {Number} optional: the amount of time (ms) to wait
	 */
	waitForPageTitle: function (ms) {
		browser.wait(function() {
			return browser.getTitle();
		}, ms || DEF_MAX_WAIT);

	},
	/**
	 * Wait synchronously for late appearing (asynchronously loaded) DOM element's text
	 * @param elm {Object} the target DOM element to wait for containing text
	 * @param [ms] {Number} optional: the amount of time (ms) to wait
	 */

	/**
	 * Wait synchronously for disappearing of an element
	 * @param elm {Object} target element to wait on
	 * @param [ms] {number} optional max amount of time to wait in ms
	 */
	waitForElementClose : function(elm, ms){
		browser.wait(function(){
			return elm.isPresent().then(function(presenceOfElement){
				return !presenceOfElement;
			});
		}, ms || DEF_MAX_WAIT);
	},

	/**
	 * Wait synchronously for a specific amount of seconds, reporting on the console
	 */
	waitVerbose : function(secs){
		browser.sleep(10).then(function(){
			console.log('waiting for',secs,'second(s)');
		}).then(this.waitTick(secs));
	},

	waitTick : function(remaining){
		browser.sleep(10).then(function(){
			if( remaining <= 5){
				process.stdout.write(remaining + "...");
				browser.sleep(1000 * remaining).then(function(){
					process.stdout.write("done.\n");
				});
			}else{
				browser.sleep(10).then(function(){
					process.stdout.write(remaining + "...");
				}).then(browser.sleep(5000).then(function(){
					global.timeModule.waitTick( remaining - 5)}));
			}
		})
	}
};
