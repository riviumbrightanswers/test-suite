'use strict';

/**
 * Supporter library for Protractor environmental integration tests
 */

/* global module, global, browser, waitsFor */
module.exports = {

	/**
	 * Get user test information from the testData.json file
	 * @returns {Object} Test user object containing login credentials (username and password)
	 */
	getTestData: function (logicalName) {
		var data = {};
		var fs = require('fs');

		var data = fs.readFileSync(__dirname + '/../testData.json', {encoding: 'utf-8'});

		var testData = JSON.parse(data);
		data = testData[logicalName];
		return data;
	}
};