'use strict';

/* global global */
describe('The log in page for Klantportaal', function() {

	var logInForm;
	var pageBaseURL 	= global.testDataModule.getTestData(global.testdata).url || global.baseUrl;
	var pagePath 		= 'mijn-gegevens/inloggen/index.xml';
    
	var customer 	    = global.testDataModule.getTestData(global.testdata).customer;
	var credentials 	= global.testDataModule.getTestData(global.testdata).login;
	var expectations 	= global.testDataModule.getTestData(global.testdata).expectations;

	beforeEach( function() {

		logInForm = new global.logInFixture();
		browser.get(pageBaseURL + pagePath);

	});

	it('should display an error when invalid credentials are entered', function() {

		global.waitForElement(logInForm.usernameField());
		global.waitForElement(logInForm.passwordField());

		logInForm.logIn('abc', '123');

		global.waitForElement(logInForm.notification());

		expect(logInForm.notification().isDisplayed()).toBe(true);
		expect(logInForm.notification().getText()).toContain('Combinatie gebruikersnaam /wachtwoord is niet bekend/onjuist.');

	});

	it('should log in the user and redirect to the \'Mijn portaal\' page when valid credentials are entered', function() {

		global.waitForElement(logInForm.usernameField());
		global.waitForElement(logInForm.passwordField());

	 	logInForm.logIn(credentials.username, credentials.password);

	 	global.wait(3000); // some extra time for the long login redirects
	 	global.waitForPageTitle();

		expect(browser.getTitle()).toBe(customer + ' - Mijn portaal');
		expect(browser.getCurrentUrl()).toBe(pageBaseURL + 'mijn-portaal/');

	 });

});