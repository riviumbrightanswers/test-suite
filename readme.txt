First install node.js and npm.

(use sudo if needed)

Then install Protractor (this has to be done globally because we need to be able to start it without Grunt):
$ npm install protractor -g

Install Selenium/Webdriver:
$ webdriver-manager update

And the additional modules defined in package.js:
$ npm install

Then run the tests with:
$ protractor protactor/config/protractor.conf.js --[parameter] [parameter value]

Parameters:	output  		Output directory for the xml file with the test results. Default is 'results/'.
			environment 	Enviroment the tests are run on. This will be used in the filename of the output xml.
			collection  	name of the test spec cllection. This will be used in the filename of the output xml.
			browser     	Browser to run the tests in. Default is Chrome. Currently supports IE, Firefox and Chrome, but running a Selenium grid may be needed for anything other than Chrome.
			suite    		Test suite to run. Not using this at the moment.
			testdata    	Test data to use. Test users and expectations are contained in testData.json. Default is "klantportaal.tst".